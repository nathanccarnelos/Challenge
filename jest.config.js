export default {
  roots: ['<rootDir>/src'],
  testMatch: ['**/?(*.)+(spec|test).+(ts|tsx|js)'],
  moduleNameMapper: {
    axios: 'axios/dist/node/axios.cjs',
  },
  transform: {
    '^.+\\.(ts|tsx)$': 'ts-jest',
  },
}
