# Challenge

### Commands

Install app

```bash
$  yarn
```

Start app

```bash
$  yarn start
```

Run Tests

```bash
$  yarn test
```

### Project

this project gets data from 2 endpoints and join tem into one,
was required to have a `Letter` Object wit a method call `get()`
to do all that process.

#### Technologies

- Typescript
- Jest
- Parcel
- Axios

#### Architecture

The idea was to use more functional programing, and something close to a repository pattern
in a front-end project, because of that Letter is a simple Object that has some methods to get the data.

Besides Letter the project has `Repository` and `Lib`

Inside repository, you can find both types of data that we need to get `User` and `Posts`, this files make the requests
passing what is need, if we had to use a cache in brower or something like that repository would be responsable
to dealing with that.

HttpRequester is a simple abstraction to use axios.

#### Template

The project uses this template that has Jest, Typescript and Parcel configured.

https://github.com/diegolameira/vanilla-challenge-template
