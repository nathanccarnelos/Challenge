import { Letter } from '../index'
import { mock } from './mock'

describe('Letter test', () => {
  it('should be able to get Users With Posts', async () => {
    Letter.load = async () => {
      return new Promise((resolve) => resolve(mock))
    }

    const usersList = await Letter.get()
    expect(usersList).toEqual([
      {
        username: 'Bret',
        website: 'hildegard.org',
        address: 'Kulas Light, Apt. 556 - 92998-3874 Gwenborough',
        company: 'Romaguera-Crona',
        email: 'Sincere@april.biz',
        id: 1,
        name: 'Leanne Graham',
        phone: '1-770-736-8031 x56442',
        posts: [
          {
            id: 1,
            title:
              'sunt aut facere repellat provident occaecati excepturi optio reprehenderit',
            body:
              'quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto',
          },
          {
            id: 2,
            title: 'qui est esse',
            body:
              'est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla',
          },
        ],
      },
    ])
  })

  it('should return error message', async () => {
    const RejectMessage = 'Test'
    Letter.load = async () => {
      return new Promise((_, reject) => reject(RejectMessage))
    }
    Letter.get().catch((e) => {
      expect(e).toEqual(RejectMessage)
    })
  })
})
