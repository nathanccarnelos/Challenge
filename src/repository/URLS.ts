export const enum URL {
  POSTS = 'https://jsonplaceholder.typicode.com/posts',
  USERS = 'https://jsonplaceholder.typicode.com/users',
}
