import { postRepository } from '../post'
import { httpRequester } from '../../lib/httpRequester/httpRequester'
import { userRepository } from '../user'

describe('repository test', () => {
  it('post getData', async () => {
    const spy = jest.spyOn(httpRequester, 'get')
    // @ts-ignore
    spy.mockReturnValue({ data: { id: 1 } })
    const response = await postRepository.getData()
    expect(response).toEqual({ id: 1 })
    spy.mockRestore()
  })

  it('user getData', async () => {
    const spy = jest.spyOn(httpRequester, 'get')
    // @ts-ignore
    spy.mockReturnValue({ data: { id: 1 } })
    const response = await userRepository.getData()
    expect(response).toEqual({ id: 1 })
    spy.mockRestore()
  })
})
