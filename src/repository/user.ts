import { httpRequester } from '../lib/httpRequester/'
import { URL } from './URLS'
import { getData } from './repositoryTypes'

export interface UserResponseType {
  id: number
  name: string
  username: string
  email: string
  address: {
    street: string
    suite: string
    city: string
    zipcode: string
    geo: {
      lat: string
      lng: string
    }
  }
  phone: string
  website: string
  company: {
    name: string
    catchPhrase: string
    bs: string
  }
}

export const userRepository: getData<UserResponseType[]> = {
  async getData() {
    const { data } = await httpRequester.get<UserResponseType[]>(URL.USERS)
    return data
  },
}
