import { httpRequester } from '../lib/httpRequester/'
import { URL } from './URLS'
import { getData } from './repositoryTypes'

export interface PostResponseType {
  userId: number
  id: number
  title: string
  body: string
}

export const postRepository: getData<PostResponseType[]> = {
  async getData() {
    const { data } = await httpRequester.get<PostResponseType[]>(URL.POSTS)
    return data
  },
}
