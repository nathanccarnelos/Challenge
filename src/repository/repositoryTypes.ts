//repository commum types

export interface getData<T = any> {
  getData(): Promise<T>
}
