import { AxiosError } from 'axios'

export const errorHandler = (Error: AxiosError): string => {
  console.error(Error)
  return `${Error?.response?.status} Message: ${Error.message}`
}
