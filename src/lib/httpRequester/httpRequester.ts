import axios, { AxiosResponse } from 'axios'

export const httpRequester = {
  async get<T>(url: string): Promise<AxiosResponse<T>> {
    return axios.get(url)
  },
}
