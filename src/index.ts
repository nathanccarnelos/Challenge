import { userRepository, UserResponseType } from './repository/user'
import { postRepository, PostResponseType } from './repository/post'
import { errorHandler } from './lib/httpRequester/'

interface UserPosts {
  id: number
  title: string
  body: string
}

interface User {
  id: number
  name: string
  username: string
  email: string
  address: string
  phone: string
  website: string
  company: string
  posts: Array<UserPosts>
}

const addressToString = ({
  street,
  suite,
  zipcode,
  city,
}: {
  street: string
  suite: string
  zipcode: string
  city: string
}) => `${street}, ${suite} - ${zipcode} ${city}`

export const Letter = {
  async load(): Promise<string | [UserResponseType[], PostResponseType[]]> {
    try {
      return await Promise.all([
        userRepository.getData(),
        postRepository.getData(),
      ])
    } catch (e) {
      return errorHandler(e)
    }
  },
  createUserPosterObj([usersData, postsData]: [
    UserResponseType[],
    PostResponseType[],
  ]): User[] {
    return usersData.map(
      (user): User => {
        const posts = postsData.reduce(
          (acc: UserPosts[], post): UserPosts[] => {
            if (post.userId !== user.id) return acc

            acc.push({
              id: post.id,
              title: post.title,
              body: post.body,
            })
            return acc
          },
          [],
        )

        return {
          id: user.id,
          name: user.name,
          username: user.username,
          email: user.email,
          address: addressToString(user.address),
          phone: user.phone,
          website: user.website,
          company: user.company.name,
          posts,
        }
      },
    )
  },
  async get(): Promise<User[] | string> {
    const requestResponse = await Letter.load()
    if (typeof requestResponse === 'string') return requestResponse
    return Letter.createUserPosterObj(requestResponse)
  },
}
function render() {
  //display on screen
  Letter.get().then((result) => {
    const elem = document.getElementById('app')
    const preElem = document.createElement('pre')
    preElem.innerText = JSON.stringify(result, null, 2)
    elem?.appendChild(preElem)
  })
}
render()
